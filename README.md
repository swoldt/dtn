## Intro

This program reads lightning data from standard input (one lightning strike per line as a JSON object, 
and matches that data against a source of assets (also in JSON format) to produce an alert.

## Note

The conversion between lat/long and quadkey is done with the help of
[Map tile library for go by buckhx](https://github.com/buckhx/tiles)

## Cody style/organization and philosophy

[Standard Package Layout](https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1)

[Go best practices, six years in](https://peter.bourgon.org/go-best-practices-2016/)

## Prequisites

    Install golang

    Setup appropriate goPath/goRoot/goBin.

    Clone this repository

## Run the project:

    make start

## Run tests only:

    make test

## Benchmark:

    make benchmark

Here are some benchmarks for converting a location at zoom level 12 @ 2.3 GHz i7


    TileFromCoordinate	                165 ns/op	                 0 B/op	         0 allocs/op
    QuadkeyFromTile   	                95 ns/op	                32 B/op	         1 allocs/op

    Benchmark-15000          51         230715867 ns/op        11192893 B/op     305619 allocs/op
    Benchmark-12500          61         189939705 ns/op         9184573 B/op     250516 allocs/op
    Benchmark-10000          80         139461189 ns/op         7313473 B/op     200199 allocs/op
    Benchmark-7500          105         116668021 ns/op         5449035 B/op     150377 allocs/op
    Benchmark-5000          181          68216673 ns/op         3675103 B/op     100052 allocs/op
    Benchmark-2500          541          21807763 ns/op         1835498 B/op      50025 allocs/op
    Benchmark-1000         1052          11521490 ns/op          742414 B/op      20028 allocs/op
    Benchmark-1          554194             21954 ns/op            4793 B/op         24 allocs/op


The benchmark indicates the program running with O(n*log(n)) - linearithmic time complexity. 
Theoretically,maps with keys from finite domains (such as ints) are O(1) in time complexity, 
and maps with keys with infinite domains (such as strings) require hashing and the specifics 
of equality testing to be included in the cost, which makes inserts and lookups best case O(N log N) 
on average (since the keys have to be at least O(log N) in size on average to construct a hash table with N entries.
Memory allocation grows linear by a factor of ~2.
