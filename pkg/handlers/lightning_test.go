package handler_test

import (
	"testing"

	"bitbucket.org/swoldt/dtn/pkg/config"
	handler "bitbucket.org/swoldt/dtn/pkg/handlers"
	"bitbucket.org/swoldt/pkg/xlogger"

	"bitbucket.org/swoldt/dtn/pkg/interfaces"
	"github.com/stretchr/testify/require"
)

var cfgPath = "./testdata/config/test_config.toml"

var MayerPark = interfaces.Lightning{
	FlashType:  1,
	Latitude:   34.7339638,
	Longitude:  -96.3070225,
	StrikeTime: 1446760903805,
}

func (s *TestSuite) Test_Validate_Event() {
	h := s.prep()

	err := h.ValidateEvent(invalid)
	require.Error(s.T(), err)

	err = h.ValidateEvent(valid)
	require.NoError(s.T(), err)
}

func (s *TestSuite) Test_Match_Event() {
	h := s.prep()
	match := h.Match(MayerPark)
	require.Equal(s.T(), true, match)
}

var defaultConfig = &xlogger.Config{
	Level:  "debug",
	Format: "text",
}

// todo https://github.com/stretchr/testify/issues/811
func Benchmark(b *testing.B) {
	cfg := new(config.AppConfig)
	err := config.LoadConfig(cfg, "./testdata/config/dev.toml")
	require.NoError(b, err)

	log, err := xlogger.New(defaultConfig)
	require.NoError(b, err)

	h, err := handler.New(log, cfg)
	require.NoError(b, err)

	for n := 0; n < b.N; n++ {
		h.Execute()
	}
}
