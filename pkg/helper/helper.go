package helper

import (
	"bitbucket.org/swoldt/pkg/xlogger"
	"github.com/stretchr/testify/suite"
)

// Helper represents the struct for this package
type Helper struct {
	suite  *suite.Suite
	logger *xlogger.Logger
}

// NewHelper returns an initiliazed helper
func NewHelper(s *suite.Suite, logger *xlogger.Logger) *Helper {
	return &Helper{suite: s, logger: logger}
}
