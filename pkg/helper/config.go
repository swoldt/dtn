package helper

import (
	"bitbucket.org/swoldt/dtn/pkg/config"
	"github.com/pelletier/go-toml"
)

// GetConfig deserializes the provided toml config path into AppConfig struct
func (h *Helper) GetConfig(configBytes []byte) (*config.AppConfig, error) {
	cfg := new(config.AppConfig)
	err := toml.Unmarshal(configBytes, cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
