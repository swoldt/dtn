package interfaces

// IHandler represents the main interface of the Application
type IHandler interface {
	Execute() error
	ValidateEvent(v Lightning) error
	LoadAssets() (map[string]Asset, error)
	ValidateAssets(assets []Asset) (map[string]Asset, error)
	Match(v Lightning) bool
}
