package config

import (
	"io/ioutil"

	"github.com/BurntSushi/toml"
	"github.com/pkg/errors"
)

// LoadConfig reads a path and unmarshalls the toml into a pointer interface of type AppConfig
func LoadConfig(cfg interface{}, path string) error {
	bytes, err := ioutil.ReadFile(path)

	if err != nil {
		return errors.Wrap(err, "unable to read file")
	}

	err = toml.Unmarshal(bytes, cfg)

	if err != nil {
		return errors.Wrapf(err, "error while parsing config file %s", string(bytes))
	}

	return nil
}

// AppConfig represents the configuration struct
type AppConfig struct {
	AssetsJSON    string `toml:"assetsJSON"`
	LightningJSON string `toml:"lightningJSON"`
	Summarize     bool   `toml:"summarize"`
}
