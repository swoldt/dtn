package interfaces

// Lightning represents the a data reading of the lightning detection sensors
type Lightning struct {
	FlashType int `json:"flashType" validate:"oneof=0 1 9"`
	// todo add custom validator to ensure strike time is appropriate
	StrikeTime      int64   `json:"strikeTime" validate:"required,gt=0"`
	Latitude        float64 `json:"latitude" validate:"required,gte=-90,lte=90"`
	Longitude       float64 `json:"longitude" validate:"required,gte=-180,lte=180"`
	PeakAmps        int     `json:"peakAmps"`
	Reserved        string  `json:"reserved"`
	IcHeight        int     `json:"icHeight"`
	ReceivedTime    int64   `json:"receivedTime"`
	NumberOfSensors int     `json:"numberOfSensors"`
	Multiplicity    int     `json:"multiplicity"`
}

// FlashType hides the real type of the enum
type FlashType = int

type flashTypeList struct {
	Cloud2Cloud  FlashType
	Cloud2Ground FlashType
	HeartBeat    FlashType
}

// FlashTypes for public use
var FlashTypes = &flashTypeList{
	Cloud2Ground: 0,
	Cloud2Cloud:  1,
	HeartBeat:    9,
}

// Asset represents the location to keep track of
type Asset struct {
	AssetName  string `json:"assetName" validate:"required"`
	QuadKey    string `json:"quadKey" validate:"required,len=12"`
	AssetOwner string `json:"assetOwner" validate:"required"`
}

// Alert represents the aggregates of lightning strikes at asset locations
type Alert struct {
	AssetName  string `json:"assetName"`
	AssetOwner string `json:"assetOwner"`
	Events     []Lightning
}
