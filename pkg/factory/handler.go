package factory

import (
	"bitbucket.org/swoldt/dtn/pkg/config"
	handler "bitbucket.org/swoldt/dtn/pkg/handlers"
	"bitbucket.org/swoldt/dtn/pkg/interfaces"
)

// InitializedHandler returns an initiliazed handler interface
func (f *Factory) InitializedHandler(cfg *config.AppConfig) (interfaces.IHandler, error) {
	return handler.New(f.logger, cfg)
}
