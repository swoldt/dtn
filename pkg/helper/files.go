package helper

import (
	"io/ioutil"
)

// BytesFromFile reads a path and returns the byte contents of that file
func (h *Helper) BytesFromFile(path string) []byte {
	bytes, err := ioutil.ReadFile(path)

	h.suite.Require().NoError(err)

	return bytes
}
