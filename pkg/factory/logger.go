package factory

import (
	"bitbucket.org/swoldt/pkg/xlogger"
	"github.com/stretchr/testify/suite"
)

// DefaultLogger returns an initiliazed logger interface
func DefaultLogger(s *suite.Suite) *xlogger.Logger {
	cfg := new(xlogger.Config)
	l, err := xlogger.New(cfg)
	s.Require().NoError(err)
	return l
}
