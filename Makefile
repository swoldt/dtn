test:
	go test ./pkg/handlers/.
	
benchmark:
	go test ./pkg/handlers/. -bench=. -benchmem -benchtime 10s

build:
	GOOS=linux GOARCH=amd64 go build -o bin/dtn cmd/dtn/main.go

# Variable for filename
PID_FILE = /tmp/dtn-app.pid

# Start task performs go run command and writes it's process id to PID_FILE.
start:
	go run cmd/dtn/main.go & echo $$! > $(PID_FILE)

# Stop task will kill process by development ApiPort
stop:
	-lsof -t -i:8989|xargs kill
  
# Restart task will execute stop and start tasks in strict order
restart: stop start
  
# Serve task will run fswatch monitor and performs restart task if any source file changed. Before serving it will execute start task.
serve: start
	fswatch -or --event=Updated ./pkg | xargs -n1 -I {} make restart

# .PHONY is used for reserving tasks words
.PHONY: test benchmark build start stop restart serve