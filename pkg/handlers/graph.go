package handler

import (
	"bitbucket.org/swoldt/dtn/pkg/interfaces"
	"fmt"
	"time"
)

// Snapshot in graph
type Snapshot struct {
	Value int
	At    time.Time
}

// summarize gives a minute by minute overview of lightning strikes
// at the location of the related asset
func (h *Handler) summarize() {
	for _, a := range h.alerts {
		h.log.Printf("%s with %d lightning events\n", a.AssetName, len(a.Events))

		var snaps []Snapshot
		total := 0
		cloud2cloudStrikes := 0
		cloud2groundStrikes := 0
		positiveStrikes := 0
		negativeStrikes := 0
		posIntensity := 0
		negIntensity := 0
		evLen := len(a.Events)
		start := time.Unix(a.Events[0].StrikeTime/1000, 0)
		end := time.Unix(a.Events[evLen-1].StrikeTime/1000, 0)

		for _, ev := range a.Events {
			now := time.Unix(ev.StrikeTime/1000, 0)
			total++

			if ev.FlashType == interfaces.FlashTypes.Cloud2Cloud {
				cloud2cloudStrikes++
			} else if ev.FlashType == interfaces.FlashTypes.Cloud2Ground {
				cloud2groundStrikes++
			}

			if ev.PeakAmps > 0 {
				positiveStrikes++
				posIntensity += ev.PeakAmps
			} else if ev.PeakAmps < 0 {
				negativeStrikes++
				negIntensity -= ev.PeakAmps
			}

			// Generate snapshots
			if now.After(start.Add(time.Minute)) || now == end {
				snap := Snapshot{
					Value: total,
					At:    start,
				}

				snaps = append(snaps, snap)
				fmt.Println(snap.At.Format("Mon 02/01/06 15:04"), snap.Value)

				// adjust/reset the values for the next snapshot
				start = start.Add(time.Minute)
				total = 0
			}
		}

		// Exploration
		if positiveStrikes > 0 {
			posPercentage := (float64(positiveStrikes) / float64(evLen)) * 100.0
			fmt.Printf("%d positive strike(s) %2.0f%% with avg intensity of %damps\n",
				positiveStrikes, posPercentage, posIntensity/positiveStrikes)
		}

		if negativeStrikes > 0 {
			negPercentage := (float64(negativeStrikes) / float64(evLen)) * 100.0
			fmt.Printf("%d negative strike(s) %2.0f%% with avg intensity of -%damps\n",
				negativeStrikes, negPercentage, negIntensity/negativeStrikes)
		}

		if cloud2cloudStrikes > 0 {
			c2cPercentage := (float64(cloud2cloudStrikes) / float64(evLen)) * 100.0
			fmt.Printf("%d cloud2cloud strike(s) %2.0f%%\n",
				cloud2cloudStrikes, c2cPercentage)
		}

		if cloud2groundStrikes > 0 {
			c2gPercentage := (float64(cloud2groundStrikes) / float64(evLen)) * 100.0
			fmt.Printf("%d cloud2ground strike(s) %2.0f%%\n",
				cloud2groundStrikes, c2gPercentage)
		}

	}
}
