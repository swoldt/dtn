package main

import (
	"flag"

	"bitbucket.org/swoldt/dtn/pkg/config"
	handler "bitbucket.org/swoldt/dtn/pkg/handlers"
	"bitbucket.org/swoldt/pkg/xlogger"
)

var defaultConfig = &xlogger.Config{
	Level:  "debug",
	Format: "text",
}

func main() {
	cfgPath := ""
	flag.StringVar(&cfgPath, "cfg", "lightning-alert/dev.toml", "Path to the config file")
	flag.Parse()

	cfg := new(config.AppConfig)
	err := config.LoadConfig(cfg, cfgPath)

	log, err := xlogger.New(defaultConfig)
	if err != nil {
		panic("Could not init log")
	}

	log.Printf("Loaded config from `%s`\n", cfgPath)

	h, err := handler.New(log, cfg)
	if err != nil {
		panic("Could not init handler")
	}

	err = h.Execute()
	if err != nil {
		panic(err)
	}

}
