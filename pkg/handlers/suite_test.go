package handler_test

import (
	"testing"

	"bitbucket.org/swoldt/dtn/pkg/factory"
	"bitbucket.org/swoldt/dtn/pkg/helper"
	"bitbucket.org/swoldt/dtn/pkg/interfaces"
	"bitbucket.org/swoldt/pkg/xlogger"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

type TestSuite struct {
	suite.Suite
	logger  *xlogger.Logger
	helper  *helper.Helper
	factory *factory.Factory
}

func (s *TestSuite) SetupSuite() {
	s.logger = factory.DefaultLogger(&s.Suite)
	s.helper = helper.NewHelper(&s.Suite, s.logger)
	s.factory = factory.NewFactory(&s.Suite, s.logger, s.helper)
}

func (s *TestSuite) prep() interfaces.IHandler {

	configBytes := s.helper.BytesFromFile(cfgPath)
	cfg, err := s.helper.GetConfig(configBytes)
	require.NoError(s.T(), err)

	h, err := s.factory.InitializedHandler(cfg)
	require.NoError(s.T(), err)
	return h
}

func TestRunner(t *testing.T) {
	suite.Run(t, new(TestSuite))
}
