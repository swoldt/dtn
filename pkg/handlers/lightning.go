package handler

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"

	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"

	"bitbucket.org/swoldt/dtn/pkg/interfaces"
	"github.com/buckhx/tiles"
)

const zoomLevel = 12

// Execute represents the main execution block
func (h *Handler) Execute() error {

	lfile, err := os.Open(h.config.LightningJSON)
	if err != nil {
		h.log.Fatal(err)
	}
	defer lfile.Close()

	scanner := bufio.NewScanner(lfile)

	for scanner.Scan() {
		v := interfaces.Lightning{}

		err := json.Unmarshal(scanner.Bytes(), &v)
		if err != nil {
			h.log.Errorln("failed to read ", err)
			continue
		}

		err = h.ValidateEvent(v)
		if err != nil {
			continue
		}

		_ = h.Match(v)
	}

	if err := scanner.Err(); err != nil {
		return errors.Wrap(err, "scan line failed")
	}

	if h.config.Summarize {
		h.summarize()
	}

	return nil
}

// ValidateEvent validates a Ligntning event
func (h *Handler) ValidateEvent(v interfaces.Lightning) error {

	if v.FlashType == interfaces.FlashTypes.HeartBeat {
		// ignoring event reading for flashType 9 heartBeat
		return errors.New("HeartBeat")
	}

	err := h.validator.Struct(&v)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			h.log.Errorf("validation for new event %s condition of %s %s %s not met with val", err.Namespace(), err.Kind(), err.Tag(), err.Param())
		}
	}

	return err
}

// Match takes a Ligntning event and tries to match it against a tracked asset
func (h *Handler) Match(v interfaces.Lightning) bool {
	t := tiles.FromCoordinate(v.Latitude, v.Longitude, zoomLevel)
	quadKey := string(t.Quadkey())

	if a, ok := h.assets[quadKey]; ok {
		if v.FlashType == interfaces.FlashTypes.Cloud2Ground ||
			v.FlashType == interfaces.FlashTypes.Cloud2Cloud {

			if _, ok := h.alerts[quadKey]; !ok {

				fmt.Printf("lightning alert for %s:%s \n", a.AssetOwner, a.AssetName)

				h.alerts[quadKey] = interfaces.Alert{
					AssetOwner: a.AssetOwner,
					AssetName:  a.AssetName,
					Events:     append(h.alerts[quadKey].Events, v),
				}
			} else {
				x := h.alerts[quadKey]
				x.Events = append(x.Events, v)
				h.alerts[quadKey] = x
			}
		}
		return true
	}
	return false
}
