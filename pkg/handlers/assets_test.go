package handler_test

import (
	"bitbucket.org/swoldt/dtn/pkg/interfaces"
	"github.com/stretchr/testify/require"
)

var valid = interfaces.Lightning{
	FlashType:  0,
	Latitude:   -90,
	Longitude:  -180,
	StrikeTime: 1446760902510,
}

var invalid = interfaces.Lightning{
	FlashType:  0,
	Latitude:   -91,
	Longitude:  -181,
	StrikeTime: 1446760902510,
}

func (s *TestSuite) Test_Load_Assets() {

	assets, err := s.prep().LoadAssets()
	require.NoError(s.T(), err)

	lenAssetsExpected := 4
	require.Equal(s.T(), lenAssetsExpected, len(assets))
}

func (s *TestSuite) Test_Validate_Assets() {
	h := s.prep()
	err := h.ValidateEvent(invalid)
	require.Error(s.T(), err)

	err = h.ValidateEvent(valid)
	require.NoError(s.T(), err)
}
