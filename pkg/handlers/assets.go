package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"bitbucket.org/swoldt/dtn/pkg/interfaces"
	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
)

// LoadAssets reads and verifies the contents of assets.json
func (h *Handler) LoadAssets() (map[string]interfaces.Asset, error) {
	assets := []interfaces.Asset{}

	afile, err := os.Open(h.config.AssetsJSON)
	if err != nil {
		return nil, errors.Wrapf(err, "open %s failed", h.config.AssetsJSON)
	}
	defer afile.Close()

	abytes, err := ioutil.ReadAll(afile)
	if err != nil {
		return nil, errors.Wrapf(err, "reading %s failed", h.config.AssetsJSON)
	}
	err = json.Unmarshal(abytes, &assets)
	if err != nil {
		return nil, errors.Wrapf(err, "unmarshalling %s failed", h.config.AssetsJSON)
	}

	return h.ValidateAssets(assets)
}

// ValidateAssets ensures that all asset properties are set valid or ignore it
func (h *Handler) ValidateAssets(assets []interfaces.Asset) (map[string]interfaces.Asset, error) {
	assetMap := map[string]interfaces.Asset{}

	for _, a := range assets {
		err := h.validator.Struct(&a)

		if err != nil {

			for _, err := range err.(validator.ValidationErrors) {
				h.log.Errorf("%s condition for asset %s of %s %s %s not met with val %s", err.Namespace(), a.AssetName, err.Kind(), err.Tag(), err.Param(), err.Value())
			}

			fmt.Printf("ignoring %s for this run \n", a.AssetName)
			continue

		}

		assetMap[a.QuadKey] = interfaces.Asset{
			AssetName:  a.AssetName,
			AssetOwner: a.AssetOwner,
		}
	}

	return assetMap, nil
}
