package handler

import (
	"bitbucket.org/swoldt/dtn/pkg/config"
	"bitbucket.org/swoldt/dtn/pkg/interfaces"
	"bitbucket.org/swoldt/pkg/xlogger"
	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
)

// Handler represents the struct for this package
type Handler struct {
	config    *config.AppConfig
	log       *xlogger.Logger
	validator *validator.Validate
	assets    map[string]interfaces.Asset
	alerts    map[string]interfaces.Alert
}

// New returns an initiliazed handler struct
func New(log *xlogger.Logger,
	cfg *config.AppConfig) (interfaces.IHandler, error) {

	h := &Handler{
		log:       log,
		config:    cfg,
		alerts:    map[string]interfaces.Alert{},
		validator: validator.New(),
	}

	assets, err := h.LoadAssets()
	if err != nil {
		return nil, errors.Wrapf(err, "loading assets")
	}

	h.assets = assets
	return h, nil
}
