module bitbucket.org/swoldt/dtn

go 1.13

require (
	bitbucket.org/swoldt/pkg v0.0.0-20191129123942-7dd98be38b1a
	github.com/BurntSushi/toml v0.3.1
	github.com/b4t3ou/cloudinary-go v0.0.0-20160805230651-93302eab8ceb
	github.com/buckhx/tiles v0.0.0-20160614171505-4994e5527da5
	github.com/friendsofgo/errors v0.9.2 // indirect
	github.com/go-chi/render v1.0.1
	github.com/go-playground/validator/v10 v10.2.0
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/imdario/mergo v0.3.9 // indirect
	github.com/namsral/flag v1.7.4-pre
	github.com/pelletier/go-toml v1.7.0
	github.com/pkg/errors v0.9.1
	github.com/pressly/chi v4.1.0+incompatible
	github.com/sirupsen/logrus v1.5.0 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/volatiletech/inflect v0.0.0-20170731032912-e7201282ae8d // indirect
	github.com/volatiletech/null v8.0.0+incompatible
	github.com/volatiletech/sqlboiler v3.7.0+incompatible // indirect
	gopkg.in/dealancer/validate.v2 v2.1.0
)
